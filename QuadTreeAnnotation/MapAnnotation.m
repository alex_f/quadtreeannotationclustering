//
//  MapAnnotation.m
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/16/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import "MapAnnotation.h"

@implementation MapAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate andCount:(int)count
{
    self = [super init];
    if(self)
    {
        self.coordinate = coordinate;
        self.count = count;
        self.title = [NSString stringWithFormat:@"%d", self.count];
    }
    return self;
}

#pragma mark - overridden methods
- (NSUInteger)hash
{
    NSString *hashString = [NSString stringWithFormat:@"%.5F%.5F", self.coordinate.latitude, self.coordinate.longitude];
    return [hashString hash];
}

- (BOOL)isEqual:(id)object
{
    return [self hash] == [object hash];
}

@end

//
//  MapClusterDataSource.h
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/21/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@protocol MapClusterDataSource <NSObject>

- (id<MKAnnotation>)mapAnnotationWithCoordinate:(CLLocationCoordinate2D)coordinate count:(int)count andData:(void *)data;
- (void *)dataForAnnotationFromDictionary:(NSDictionary *)dict;

@end

//
//  ClusterCoordinateQuadTree.h
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/21/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuadTree.h"
#import "MapClusterDataSource.h"

@class MKMapView;

@interface ClusterCoordinateQuadTree : NSObject
{
    QuadTreeNode    *_root;
}

@property (nonatomic, weak) MKMapView *mapView;
@property (nonatomic, weak) id<MapClusterDataSource> dataSource;

- (void)buildTreeWithArray:(NSArray *)dataArray;
- (NSArray *)clusteredAnnotations;

@end

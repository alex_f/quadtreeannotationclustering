//
//  ClusterCoordinateQuadTree.m
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/21/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import "ClusterCoordinateQuadTree.h"
#import <MapKit/MapKit.h>

float CellSizeForZoomScale(MKZoomScale zoomScale)
{
    // zoom scale to zoom level
    double totalTilesAtMaxZoom = MKMapSizeWorld.width / 256.0f;
    NSInteger zoomLevelAtMaxZoom = log2(totalTilesAtMaxZoom);
    NSInteger zoomLevel = MAX(0, zoomLevelAtMaxZoom + floor(log2f(zoomScale) + 0.5f));
    
    switch(zoomLevel)
    {
        case 13:
        case 14:
        case 15:
            return 64.0;
        case 16:
        case 17:
        case 18:
            return 32.0;
        case 19:
            return 16.0;
        default:
            return 88.0;
    }
}

BoundingBox BoundingBoxForMapRect(MKMapRect mapRect)
{
    CLLocationCoordinate2D topLeft = MKCoordinateForMapPoint(mapRect.origin);
    CLLocationCoordinate2D botRight = MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMaxX(mapRect), MKMapRectGetMaxY(mapRect)));
    return BoundingBoxMake(botRight.latitude, topLeft.longitude, topLeft.latitude, botRight.longitude);
}

@implementation ClusterCoordinateQuadTree

- (void)buildTreeWithArray:(NSArray *)dataArray
{
    @autoreleasepool {
        int i = 0;
        int count = (int)[dataArray count];
        QuadTreeNodeData *data = malloc(sizeof(QuadTreeNodeData) * count);
        for(i = 0; i < count; i++)
        {
            NSDictionary *dict = dataArray[i];
            double latitude = [dict[@"Latitude"] doubleValue];
            double longitude = [dict[@"Longitude"] doubleValue];
            void *annData = [self.dataSource dataForAnnotationFromDictionary:dict];
            data[i] = QuadTreeNodeDataMake(latitude, longitude, annData);
        }
        
        BoundingBox world = BoundingBoxForMapRect(MKMapRectWorld);
        _root = QuadTreeBuildWithData(data, count, world, 4);
    }
}

- (NSArray *)clusteredAnnotations
{
    double scale = CGRectGetWidth(self.mapView.bounds) / MKMapRectGetWidth(self.mapView.visibleMapRect);
    double cellSize = CellSizeForZoomScale(scale);
    double scaleFactor = scale / cellSize;
    
    NSInteger minX = floor(MKMapRectGetMinX(self.mapView.visibleMapRect) * scaleFactor);
    NSInteger maxX = floor(MKMapRectGetMaxX(self.mapView.visibleMapRect) * scaleFactor);
    NSInteger minY = floor(MKMapRectGetMinY(self.mapView.visibleMapRect) * scaleFactor);
    NSInteger maxY = floor(MKMapRectGetMaxY(self.mapView.visibleMapRect) * scaleFactor);
    
    NSInteger x = 0, y = 0;
    NSMutableArray *clusteredAnnotations = [NSMutableArray new];
    for(x=minX; x<=maxX; x++)
    {
        for(y=minY; y<=maxY; y++)
        {
            MKMapRect mapRect = MKMapRectMake(x/scaleFactor, y/scaleFactor, 1.0/scaleFactor, 1.0/scaleFactor);
            
            __block double totalX = 0.0;
            __block double totalY = 0.0;
            __block int count = 0;
            __block void *data = 0;
            
            QuadTreeGatherDataInRange(_root, BoundingBoxForMapRect(mapRect), ^(QuadTreeNodeData nodeData) {
                totalX += nodeData.x;
                totalY += nodeData.y;
                data = nodeData.data;
                count++;
            });
            
            if(count >= 1)
            {
                CLLocationCoordinate2D coordinate = count == 1 ? CLLocationCoordinate2DMake(totalX, totalY) : CLLocationCoordinate2DMake(totalX/count, totalY/count);
                
                if([self.dataSource respondsToSelector:@selector(mapAnnotationWithCoordinate:count:andData:)])
                    [clusteredAnnotations addObject:[self.dataSource mapAnnotationWithCoordinate:coordinate count:count andData:data]];
                }
        }
    }
    
    return [NSArray arrayWithArray:clusteredAnnotations];
}

@end

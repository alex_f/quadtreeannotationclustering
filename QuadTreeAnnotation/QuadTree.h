//
//  QuadTree.h
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/16/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct QuadTreeNodeData
{
    double  x;
    double  y;
    void    *data;
} QuadTreeNodeData;

typedef struct BoundingBox
{
    double x0;
    double y0;
    double xf;
    double yf;
} BoundingBox;

typedef struct quadTreeNode
{
    struct quadTreeNode *northWest;
    struct quadTreeNode *northEast;
    struct quadTreeNode *southWest;
    struct quadTreeNode *southEast;
    BoundingBox         boundingBox;
    int                 bucketCapacity;
    QuadTreeNodeData    *points;
    int                 count;
} QuadTreeNode;

typedef void(^DataReturnBlock)(QuadTreeNodeData nodeData);
typedef void(^QuadTreeTraverseBlock)(QuadTreeNode *node);

QuadTreeNodeData QuadTreeNodeDataMake(double x, double y, void *data);

BoundingBox BoundingBoxMake(double x0, double y0, double xf, double yf);
bool BoundingBoxContainsData(BoundingBox bBox, QuadTreeNodeData nodeData);
bool BoundingBoxIntersectBoundingBox(BoundingBox box1, BoundingBox box2);

QuadTreeNode *QuadTreeNodeMake(BoundingBox boundary, int bucketCapacity);
QuadTreeNode *QuadTreeBuildWithData(QuadTreeNodeData *data, int count, BoundingBox boundingBox, int capacity);
void FreeQuadTreeNode(QuadTreeNode *node);
void QuadTreeNodeSubdivide(QuadTreeNode *node);
bool QuadTreeNodeInsertData(QuadTreeNode *node, QuadTreeNodeData data);
void QuadTreeGatherDataInRange(QuadTreeNode *node, BoundingBox bBox, DataReturnBlock block);
void QuadTreeTraverse(QuadTreeNode *node, QuadTreeTraverseBlock block);

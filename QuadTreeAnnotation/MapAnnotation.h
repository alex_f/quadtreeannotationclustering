//
//  MapAnnotation.h
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/16/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnnotation : NSObject <MKAnnotation>

@property (nonatomic) int count;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subtitle;
@property (nonatomic) CLLocationCoordinate2D coordinate;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate andCount:(int)count;

@end

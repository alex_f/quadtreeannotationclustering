//
//  ViewController.h
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/15/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

typedef struct AirportInfo
{
    char *city;
    char *code;
} AirportInfo;

@interface ViewController : UIViewController <MKMapViewDelegate>
{
    IBOutlet MKMapView  *_mapView;
    NSArray             *_airportsArray;
}

@end

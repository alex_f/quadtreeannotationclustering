//
//  QuadTree.m
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/16/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import "QuadTree.h"

#pragma mark - constructors
QuadTreeNodeData QuadTreeNodeDataMake(double x, double y, void *data)
{
    QuadTreeNodeData nodeData;
    nodeData.x = x;
    nodeData.y = y;
    nodeData.data = data;
    return nodeData;
}

BoundingBox BoundingBoxMake(double x0, double y0, double xf, double yf)
{
    BoundingBox bBox;
    bBox.x0 = x0;
    bBox.y0 = y0;
    bBox.xf = xf;
    bBox.yf = yf;
    return bBox;
}

QuadTreeNode *QuadTreeNodeMake(BoundingBox boundary, int bucketCapacity)
{
    QuadTreeNode *node = malloc(sizeof(QuadTreeNode));
    node->northWest = NULL;
    node->northEast = NULL;
    node->southEast = NULL;
    node->southWest = NULL;
    node->boundingBox = boundary;
    node->bucketCapacity = bucketCapacity;
    node->points = malloc(sizeof(QuadTreeNode) * bucketCapacity);
    return node;
}

#pragma mark - BoundingBox Functions
bool BoundingBoxContainsData(BoundingBox bBox, QuadTreeNodeData nodeData)
{
    bool containsX = (bBox.x0 <= nodeData.x) && (nodeData.x <= bBox.xf);
    bool containsY = (bBox.y0 <= nodeData.y) && (nodeData.y <= bBox.yf);
    return containsX && containsY;
}

bool BoundingBoxIntersectBoundingBox(BoundingBox box1, BoundingBox box2)
{
    return box1.x0 <= box2.xf && box1.xf >= box2.x0 && box1.y0 <= box2.yf && box1.xf >= box2.x0;
}

#pragma mark - QuadTree Functions
void QuadTreeNodeSubdivide(QuadTreeNode *node)
{
    BoundingBox bBox = node->boundingBox;
    
    double xMid = (bBox.xf + bBox.x0) / 2.0;
    double yMid = (bBox.yf + bBox.y0) / 2.0;
    
    BoundingBox northWest = BoundingBoxMake(bBox.x0, bBox.y0, xMid, yMid);
    node->northWest = QuadTreeNodeMake(northWest, node->bucketCapacity);
    
    BoundingBox northEast = BoundingBoxMake(xMid, bBox.y0, bBox.xf, yMid);
    node->northEast = QuadTreeNodeMake(northEast, node->bucketCapacity);
    
    BoundingBox southWest = BoundingBoxMake(bBox.x0, yMid, xMid, bBox.yf);
    node->southWest = QuadTreeNodeMake(southWest, node->bucketCapacity);
    
    BoundingBox southEast = BoundingBoxMake(xMid, yMid, bBox.xf, bBox.yf);
    node->southEast = QuadTreeNodeMake(southEast, node->bucketCapacity);
}

bool QuadTreeNodeInsertData(QuadTreeNode *node, QuadTreeNodeData data)
{
    if(!BoundingBoxContainsData(node->boundingBox, data))
        return false;
    
    if(node->count < node->bucketCapacity)
    {
        node->points[node->count++] = data;
        return true;
    }
    
    if(node->northWest == NULL)
        QuadTreeNodeSubdivide(node);
    
    if(QuadTreeNodeInsertData(node->northWest, data))
        return true;
    if(QuadTreeNodeInsertData(node->northEast, data))
        return true;
    if(QuadTreeNodeInsertData(node->southWest, data))
        return true;
    if(QuadTreeNodeInsertData(node->southEast, data))
        return true;
    
    return false;
}
void QuadTreeGatherDataInRange(QuadTreeNode *node, BoundingBox bBox, DataReturnBlock block)
{
    if(!BoundingBoxIntersectBoundingBox(node->boundingBox, bBox))
        return;
    
    int i = 0;
    for(i=0; i<node->count; i++)
    {
        if(BoundingBoxContainsData(bBox, node->points[i]))
            block(node->points[i]);
    }
    
    if(node->northWest == NULL)
        return;
    
    QuadTreeGatherDataInRange(node->northWest, bBox, block);
    QuadTreeGatherDataInRange(node->northEast, bBox, block);
    QuadTreeGatherDataInRange(node->southWest, bBox, block);
    QuadTreeGatherDataInRange(node->southEast, bBox, block);
}

void QuadTreeTraverse(QuadTreeNode *node, QuadTreeTraverseBlock block)
{
    block(node);
    
    if(node->northWest == NULL)
        return;
    
    QuadTreeTraverse(node->northWest, block);
    QuadTreeTraverse(node->northEast, block);
    QuadTreeTraverse(node->southWest, block);
    QuadTreeTraverse(node->southEast, block);
}

QuadTreeNode *QuadTreeBuildWithData(QuadTreeNodeData *data, int count, BoundingBox boundingBox, int capacity)
{
    int i = 0;
    QuadTreeNode *root = QuadTreeNodeMake(boundingBox, capacity);
    for(i=0; i<count; i++)
        QuadTreeNodeInsertData(root, data[i]);
    return root;
}

void FreeQuadTreeNode(QuadTreeNode *node)
{
    if(node->northWest != NULL)
        FreeQuadTreeNode(node->northWest);
    if(node->northEast != NULL)
        FreeQuadTreeNode(node->northEast);
    if(node->southWest != NULL)
        FreeQuadTreeNode(node->southWest);
    if(node->southEast != NULL)
        FreeQuadTreeNode(node->southEast);
    
    int i = 0;
    for(i=0; i<node->count; i++)
        free(node->points[i].data);
    
    free(node->points);
    free(node);
}

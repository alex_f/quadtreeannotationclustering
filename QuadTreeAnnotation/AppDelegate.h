//
//  AppDelegate.h
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/15/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  ViewController.m
//  QuadTreeAnnotation
//
//  Created by Alex Fedko on 4/15/14.
//  Copyright (c) 2014 TNLabs. All rights reserved.
//

#import "ViewController.h"
#import "MapAnnotation.h"
#import "ClusterCoordinateQuadTree.h"
#import "MapClusterDataSource.h"

@interface ViewController () <MapClusterDataSource>
{
    ClusterCoordinateQuadTree  *_clusterQuadTree;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
//    NSMutableArray *loadArray = [NSMutableArray new];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Airports" ofType:@"plist"];
    NSArray *list = (NSArray *)[NSPropertyListSerialization propertyListFromData:[NSData dataWithContentsOfFile:path]
                                                                mutabilityOption:NSPropertyListImmutable
                                                                          format:nil
                                                                errorDescription:nil];
//    [list enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
//        MapAnnotation *annotation = [[MapAnnotation alloc] initWithCode:obj[@"Code"]
//                                                                   city:obj[@"City"]
//                                                              longitude:[obj[@"Longitude"] floatValue]
//                                                               latitude:[obj[@"Latitude"] floatValue]];
//        [loadArray addObject:annotation];
//    }];
    _airportsArray = [[NSArray alloc] initWithArray:list];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _clusterQuadTree = [[ClusterCoordinateQuadTree alloc] init];
    _clusterQuadTree.mapView = _mapView;
    _clusterQuadTree.dataSource = self;
    [_clusterQuadTree buildTreeWithArray:_airportsArray];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    [[NSOperationQueue new] addOperationWithBlock:^{
        NSArray *annotations = [_clusterQuadTree clusteredAnnotations];
        NSMutableSet *before = [NSMutableSet setWithArray:_mapView.annotations];
        [before removeObject:[_mapView userLocation]];
        NSSet *after = [NSSet setWithArray:annotations];
        
        NSMutableSet *toKeep = [NSMutableSet setWithSet:before];
        [toKeep intersectSet:after];
        
        NSMutableSet *toAdd = [NSMutableSet setWithSet:after];
        [toAdd minusSet:toKeep];
        
        NSMutableSet *toRemove = [NSMutableSet setWithSet:before];
        [toRemove minusSet:after];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [_mapView removeAnnotations:[toRemove allObjects]];
            [_mapView addAnnotations:[toAdd allObjects]];
        }];
    }];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"mapAnnotationView"];
    annotationView.pinColor = [(MapAnnotation *)annotation count] == 1 ? MKPinAnnotationColorRed : MKPinAnnotationColorGreen;
    annotationView.animatesDrop = YES;
    annotationView.canShowCallout = YES;
    [annotationView setSelected:YES animated:YES];
    return annotationView;
}

#pragma mark - MapClusterDataSource
- (id<MKAnnotation>)mapAnnotationWithCoordinate:(CLLocationCoordinate2D)coordinate count:(int)count andData:(void *)data
{
    MapAnnotation *annotation = [[MapAnnotation alloc] initWithCoordinate:coordinate andCount:count];
    if(count == 1)
    {
        AirportInfo info = *(AirportInfo *)data;
        annotation.title = [NSString stringWithFormat:@"%s", info.city];
        annotation.subtitle = [NSString stringWithUTF8String:info.code];
    }
    return annotation;
}

- (void *)dataForAnnotationFromDictionary:(NSDictionary *)dict
{
    AirportInfo *info = malloc(sizeof(AirportInfo));
    
    NSString *city = [dict[@"City"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    info->city = malloc(sizeof(char) * [city length] + 1);
    strncpy(info->city, [city UTF8String], [city length] + 1);
    
    NSString *code = [dict[@"Code"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    info->code = malloc(sizeof(char) * [code length] + 1);
    strncpy(info->code, [code UTF8String], [code length] + 1);
    
    return info;
}

@end
